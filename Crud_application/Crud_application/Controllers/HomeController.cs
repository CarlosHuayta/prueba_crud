﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Crud_application.Models;
using Crud_application.Models.ViewTableModels;
using Crud_application.Models.AddTableModel;

namespace Crud_application.Controllers
{
    public class HomeController : Controller
    {

        private testEntities7 comp = new testEntities7();
        public ActionResult Index()
        {
            ViewBag.ListadoCompanias = ObtenerCompanias();
            ViewBag.ListadoPersonas = ObtenerPersonas();
            ViewBag.ListadoPropiedades = ObtenerPropiedades();
            ViewBag.ListadoIngresos = ObtenerIngresos();
            ViewBag.ListadoSubcuentas = ObtenerSubcuentas();
            ViewBag.ListadoCajaEfectivos = ObtenerCajaEfectivos();
            ViewBag.ListadoBancos = ObtenerBancos();
            return View();
        }

        public List<SelectListItem> ObtenerCompanias()
        {
            List<SelectListItem> x = new List<SelectListItem>();
            using (testEntities7 db = new testEntities7())
            {
                var lst = db.Campania;
                
                foreach (var oComp in lst)
                {
                    x.Add(new SelectListItem() { Text = oComp.Nombre, Value = oComp.Id.ToString() });
                }
            
            }
            return x ;
         }
        public List<SelectListItem> ObtenerPersonas()
        {
            List<SelectListItem> x = new List<SelectListItem>();
            using (testEntities7 db = new testEntities7())
            {
                var lst = db.Personas;

                foreach (var oPer in lst)
                {
                    x.Add(new SelectListItem() { Text = oPer.Nombres, Value = oPer.Id.ToString() });
                }

            }
            return x;
        }
        public List<SelectListItem> ObtenerPropiedades()
        {
            List<SelectListItem> x = new List<SelectListItem>();
            using (testEntities7 db = new testEntities7())
            {
                var lst = db.Propiedades;

                foreach (var oProp in lst)
                {
                    x.Add(new SelectListItem() { Text = oProp.Descripcion, Value = oProp.Id.ToString() });
                }

            }
            return x;
        }
        public List<SelectListItem> ObtenerIngresos()
        {
            List<SelectListItem> x = new List<SelectListItem>();
            using (testEntities7 db = new testEntities7())
            {
                var lst = db.Ingresos;

                foreach (var oIngr in lst)
                {
                    x.Add(new SelectListItem() { Text = oIngr.DescripcionMovimiento, Value = oIngr.Id.ToString() });
                }

            }
            return x;
        }

        public List<SelectListItem> ObtenerSubcuentas()
        {
            List<SelectListItem> x = new List<SelectListItem>();
            using (testEntities7 db = new testEntities7())
            {
                var lst = db.Subcuentas;

                foreach (var oSub in lst)
                {
                    x.Add(new SelectListItem() { Text = oSub.Descripcion, Value = oSub.Codigo });
                }

            }
            return x;
        }
        public List<SelectListItem> ObtenerCajaEfectivos()
        {
            List<SelectListItem> x = new List<SelectListItem>();
            using (testEntities7 db = new testEntities7())
            {
                var lst = db.CajaEfectivo;

                foreach (var oCaj in lst)
                {
                    x.Add(new SelectListItem() { Text = oCaj.Descripcion, Value = oCaj.Id.ToString() });
                }

            }
            return x;
        }
        public List<SelectListItem> ObtenerBancos()
        {
            List<SelectListItem> x = new List<SelectListItem>();
            using (testEntities7 db = new testEntities7())
            {
                var lst = db.Bancos;

                foreach (var oBanc in lst)
                {
                    x.Add(new SelectListItem() { Text = oBanc.Descripcion, Value = oBanc.Id.ToString() });
                }

            }
            return x;
        }

        public ActionResult getdata()
        {
            List<IngresoTableViewModel> lst = null;
            List<CompaniaTableViewModel> compania = null;
            using (testEntities7 db = new testEntities7())
            {
                lst = (from d in db.Ingresos
                       select new IngresoTableViewModel
                       {
                           Id = d.Id,
                           DescripcionMovimiento = d.DescripcionMovimiento,
                           FechaRegistro = d.FechaRegistro,
                           Monto = d.Monto,
                           MonedaIso = d.MonedaIso,
                           FormaIngreso = d.FormaIngreso,
                           Estado = d.Estado,
                           NroCheque = d.NroCheque,
                           NroLanzamiento = d.NroCheque,
                           PathComprabante = d.PathComprobante
                       }).ToList();
            }
          
            return Json(new { data = lst }, JsonRequestBehavior.AllowGet);
         }

        public JsonResult postdata(Ingresos ob_C) //insertar nuevo y actualizar
        {
            if (ob_C.Id > 0)
            {
                using (var db = new testEntities7()) {

                    Ingresos crud = db.Ingresos.Where(model => model.Id == ob_C.Id).FirstOrDefault<Ingresos>();
                    crud.IdBanco = ob_C.IdBanco;
                    crud.IdCajaEfectivo = ob_C.IdCajaEfectivo;
                    crud.IdCampania = ob_C.IdCampania;
                    crud.IdPersonaModificacion = ob_C.IdPersonaModificacion;
                    crud.IdPersonaRegistro = ob_C.IdPersonaRegistro;
                    crud.IdPersonaVenta = ob_C.IdPersonaVenta;
                    crud.IdPropiedad = ob_C.IdPropiedad;
                    crud.MonedaIso = ob_C.MonedaIso;
                    crud.Monto = ob_C.Monto;
                    crud.NroCheque = ob_C.NroCheque;
                    crud.NroLanzamiento = ob_C.NroLanzamiento;
                    crud.PathComprobante = ob_C.PathComprobante;
                    crud.PathComprobante = ob_C.PathComprobante;
                    db.SaveChanges();
                }
                return Json(new { result = true, message = "Data Updated successfully.." }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                using (var db = new testEntities7())
                {
                    Ingresos crud = new Ingresos();
                    crud.IdBanco = ob_C.IdBanco;
                    crud.IdCajaEfectivo = ob_C.IdCajaEfectivo;
                    crud.IdCampania = ob_C.IdCampania;
                    crud.IdPersonaModificacion = ob_C.IdPersonaModificacion;
                    crud.IdPersonaRegistro = ob_C.IdPersonaRegistro;
                    crud.IdPersonaVenta = ob_C.IdPersonaVenta;
                    crud.IdPropiedad = ob_C.IdPropiedad;
                    crud.MonedaIso = ob_C.MonedaIso;
                    crud.Monto = ob_C.Monto;
                    crud.NroCheque = ob_C.NroCheque;
                    crud.NroLanzamiento = ob_C.NroLanzamiento;
                    crud.PathComprobante = ob_C.PathComprobante;
                    crud.PathComprobante = ob_C.PathComprobante;
                    db.Ingresos.Add(ob_C);
                    db.SaveChanges();
                }
                return Json(new { result = true, message = "Data saved successfully.." }, JsonRequestBehavior.AllowGet);
            }
            
        }
        
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}