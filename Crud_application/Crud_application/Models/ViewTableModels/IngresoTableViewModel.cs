﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Crud_application.Models.ViewTableModels
{
    public class IngresoTableViewModel
    {
        public int Id { get; set; }
        public String DescripcionMovimiento { get; set; }
        public System.DateTime FechaRegistro { get; set; }
        public Decimal Monto { get; set; }
        public String MonedaIso { get; set; }
        public int FormaIngreso { get; set; }
        public int Estado { get; set; }
        public String NroCheque { get; set; }
        public String NroLanzamiento { get; set; }
        public String PathComprabante { get; set; }
    }
}