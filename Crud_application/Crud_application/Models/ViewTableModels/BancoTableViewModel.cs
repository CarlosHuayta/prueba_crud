﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Crud_application.Models.ViewTableModels
{
    public class BancoTableViewModel
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
    }
}