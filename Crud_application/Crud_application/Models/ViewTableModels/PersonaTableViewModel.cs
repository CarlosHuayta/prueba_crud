﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Crud_application.Models.ViewTableModels
{
    public class PersonaTableViewModel
    {
        public int Id { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
    }
}