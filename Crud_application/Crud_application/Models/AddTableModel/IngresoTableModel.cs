﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Crud_application.Models.AddTableModel
{
    public class IngresoTableModel
    {
        public int Id { get; set; }
        public string DescripcionMovimiento { get; set; }
        public int IdCampania { get; set; }
        public int IdPersonaRegistro { get; set; }
        public System.DateTime FechaRegistro { get; set; }
        public decimal Monto { get; set; }
        public string MonedaIso { get; set; }
        public int IdPropiedad { get; set; }
        public int FormaIngreso { get; set; }
        public string CodSubcuenta { get; set; }
        public int IdPersonaVenta { get; set; }
        public int Estado { get; set; }
        public System.DateTime FechaModificacion { get; set; }
        public Nullable<int> IdPersonaModificacion { get; set; }
        public Nullable<int> IdCajaEfectivo { get; set; }
        public Nullable<int> IdBanco { get; set; }
        public string NroCheque { get; set; }
        public string NroLanzamiento { get; set; }
        public string PathComprobante { get; set; }
    }
}