﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CrudPrueba.Models;
using CrudPrueba.Models.TableViewModels;

namespace CrudPrueba.Controllers
{
    // testEntities ob = new testEntities();

    public class IngresoController : Controller
    {
        // GET: Ingreso
        public ActionResult Index()
        {
            List<IngresoTableViewModel> lst = null;
            using (testEntities db = new testEntities())
            {
                lst = (from d in db.Ingresos
                       select new IngresoTableViewModel
                       {
                           Id = d.Id,
                           DescripcionMovimiento = d.DescripcionMovimiento,
                           FechaRegistro = d.FechaRegistro,
                           Monto = d.Monto,
                           MonedaIso = d.MonedaIso,
                           FormaIngreso = d.FormaIngreso,
                           Estado = d.Estado,
                           NroCheque = d.NroCheque,
                           NroLanzamiento = d.NroCheque,
                           PathComprabante = d.PathComprobante
                       }).ToList();
            }
            return View(lst);
        }
    }
}